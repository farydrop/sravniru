package com.example.sravniru

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sravniru.adapter.CoeffAdapter
import com.example.sravniru.model.coeffData
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog


class MainActivity : AppCompatActivity(), ItemClickListener {
    private var coeffAdapter: CoeffAdapter = CoeffAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val inputs1 = findViewById<TextView>(R.id.inputs1)
        inputs1.setOnClickListener { showBottomSheetDialog() }

        /**val editText1 = findViewById<EditText>(R.id.bsf_inputs_1)
        val nxtBtn1 = findViewById<TextView>(R.id.nxtBtn1)
        nxtBtn1.setOnClickListener {inputs1.text = editText1.text.toString()}**/



        val inputs2 = findViewById<TextView>(R.id.inputs2)
        inputs2.setOnClickListener { showBottomSheetDialogSecond() }

        val inputs3 = findViewById<TextView>(R.id.inputs3)
        inputs3.setOnClickListener { showBottomSheetDialogThird() }

        val inputs4 = findViewById<TextView>(R.id.inputs4)
        inputs4.setOnClickListener { showBottomSheetDialogFor() }

        val inputs5 = findViewById<TextView>(R.id.inputs5)
        inputs5.setOnClickListener { showBottomSheetDialogFive() }

        val inputs6 = findViewById<TextView>(R.id.inputs6)
        inputs6.setOnClickListener { showBottomSheetDialogSix() }


        /**val inputs2 = findViewById<TextView>(R.id.inputs2)
        inputs2.setOnClickListener { openBottomSheet() }**/


        /**val title: TextView = findViewById(R.id.bsf_title)
        title.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?): Unit {
                title.text = getString(R.string.inputs_2)
            }
        })**/


        /**var imageButton: ImageButton = findViewById<View>(R.id.imageView) as ImageButton
        var flag = true

        imageButton.setOnClickListener { // меняем изображение на кнопке
            if (flag) {
                imageButton.setImageResource(R.drawable.coeff_info1)  // возвращаем первую картинку
            } else
                // возвращаем первую картинку
                imageButton.setImageResource(R.drawable.coeff_info2)
            flag = !flag
        }**/
        val rc = findViewById<RecyclerView>(R.id.recyclerview)
        val btn = findViewById<RelativeLayout>(R.id.coeff_info_close)
        btn.setOnClickListener {
            rc.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            rc.adapter = coeffAdapter
            coeffAdapter.addAll(coeffData)
        }


    }

    private fun showBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(this)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet)
        bottomSheetDialog.findViewById<RelativeLayout>(R.id.bsf_1)

        bottomSheetDialog.show()
    }
    private fun showBottomSheetDialogSecond() {
        val bottomSheetDialogSecond = BottomSheetDialog(this)
        bottomSheetDialogSecond.setContentView(R.layout.bottom_sheet_2)
        bottomSheetDialogSecond.findViewById<RelativeLayout>(R.id.bsf_2)
        bottomSheetDialogSecond.show()
    }
    private fun showBottomSheetDialogThird() {
        val bottomSheetDialogThird = BottomSheetDialog(this)
        bottomSheetDialogThird.setContentView(R.layout.bottom_sheet_3)
        bottomSheetDialogThird.findViewById<RelativeLayout>(R.id.bsf_3)
        bottomSheetDialogThird.show()
    }
    private fun showBottomSheetDialogFor() {
        val bottomSheetDialogFor = BottomSheetDialog(this)
        bottomSheetDialogFor.setContentView(R.layout.bottom_sheet_4)
        bottomSheetDialogFor.findViewById<RelativeLayout>(R.id.bsf_4)
        bottomSheetDialogFor.show()
    }
    private fun showBottomSheetDialogFive() {
        val bottomSheetDialogFive = BottomSheetDialog(this)
        bottomSheetDialogFive.setContentView(R.layout.bottom_sheet_5)
        bottomSheetDialogFive.findViewById<RelativeLayout>(R.id.bsf_5)
        bottomSheetDialogFive.show()
    }
    private fun showBottomSheetDialogSix() {
        val bottomSheetDialogSix = BottomSheetDialog(this)
        bottomSheetDialogSix.setContentView(R.layout.bottom_sheet_6)
        bottomSheetDialogSix.findViewById<RelativeLayout>(R.id.bsf_6)
        bottomSheetDialogSix.show()
    }



    /**private fun openBottomSheet(){
        val addPhotoBottomDialogFragment = ActionBottom.newInstance()
        addPhotoBottomDialogFragment.show(
            supportFragmentManager,ActionBottom.TAG
        )
    }**/

    override fun onItemClick(item: String?) {
    }

}