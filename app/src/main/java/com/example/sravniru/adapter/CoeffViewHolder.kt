package com.example.sravniru.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sravniru.R
import com.example.sravniru.model.CoeffModel

class CoeffViewHolder(private val root: View): RecyclerView.ViewHolder(root) {

    fun bind(coeff: CoeffModel){
        root.findViewById<TextView>(R.id.coeff_1).text = coeff.title
        root.findViewById<TextView>(R.id.inBrcts).text = coeff.addInfo
        root.findViewById<TextView>(R.id.inf).text = coeff.sumInfo
        root.findViewById<TextView>(R.id.coeff).text = coeff.coeff
    }
}