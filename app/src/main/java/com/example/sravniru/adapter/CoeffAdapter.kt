package com.example.sravniru.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sravniru.MainActivity
import com.example.sravniru.R
import com.example.sravniru.model.CoeffModel


class CoeffAdapter : RecyclerView.Adapter<CoeffViewHolder>(){
    private var coeffList: MutableList<CoeffModel> = mutableListOf<CoeffModel>()

    //создает экземпляр класса RecyclerView.ViewHolder и создает разметку
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoeffViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.coeff_info_open, parent, false)
        return CoeffViewHolder(layoutInflater)
    }

    //кол-во элементов которое мы хотим отобразить
    override fun getItemCount(): Int {
        return coeffList.size
    }

    //привязывает данные к ViewHolder
    override fun onBindViewHolder(holder: CoeffViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getItem(position: Int): CoeffModel{
        return coeffList[position]
    }

    fun addAll(generlist: List<CoeffModel>){
        this.coeffList.clear()
        this.coeffList.addAll(generlist)
        notifyDataSetChanged()
    }


}